# Backstage Demo for Digital Rebar Integration

Depends on node 18. If you have nvm, you can `nvm install 18 && nvm use 18`

## Pre-made Walkthrough (seems to be broken)

1. Install yarn: `npm i -g yarn`
1. Navigate to backstage dir: `cd backstage`
1. Generate a token with `drpcli users token rocketskates ttl 9999999 | jq .Token -r` (ttl is in seconds)
1. Populate `app-config.local.yaml` with:
    ```yaml
    digitalrebar:
      endpoint: 127.0.0.1:8092
      token: REPLACE_ME
    ```
1. Install dependencies: `yarn install`
1. Run backstage: `yarn dev`
1. Open http://127.0.0.1:3000 in browser (not `localhost`)

## Manual Walkthrough

> This entire walkthrough is automated with [`./walkthrough.sh`](walkthrough.sh). Make sure you change the variables at the top before running.

1. Install yarn: `npm i -g yarn`
1. Create backstage app: `npx @backstage/create-app@latest` (press enter)
1. Navigate to newly created `backstage` dir: `cd backstage`
1. Initialize git repo: `git init` (this can be skipped if you don't plan on using the `curl https://url | git apply` commands)
1. Clone DRP example backend plugin: `git clone https://gitlab.com/zfranks/backstage-plugin-digitalrebar-backend plugins/backstage-plugin-digitalrebar-backend`
1. Add it to the packages: `yarn add --cwd packages/backend @rackn/plugin-digitalrebar-backend@0.1.0`
1. Create `packages/backend/src/plugins/digitalrebar.ts` with

    Via curl: `curl -o packages/backend/src/plugins/digitalrebar.ts https://gitlab.com/Meshiest/backstage-drp-demo/-/raw/main/backstage/packages/backend/src/plugins/digitalrebar.ts`

    Manually:
    ```ts
    import { createRouter } from '@rackn/plugin-digitalrebar-backend';
    import { Router } from 'express';
    import { PluginEnvironment } from '../types';

    export default async function createPlugin(
      env: PluginEnvironment,
    ): Promise<Router> {
      return await createRouter({ logger: env.logger, config: env.config });
    }
    ```
1. Update `packages/backend/src/plugins/scaffolder.ts`

    Via curl: `curl https://gitlab.com/Meshiest/backstage-drp-demo/-/raw/main/patches/1-scaffolder.patch | git apply`

    Manually:
    ```diff
     import { CatalogClient } from '@backstage/catalog-client';
    +import { ScmIntegrations } from '@backstage/integration';
    -import { createRouter } from '@backstage/plugin-scaffolder-backend';
    +import { createBuiltinActions, createRouter } from '@backstage/plugin-scaffolder-backend';
     import { Router } from 'express';
     import type { PluginEnvironment } from '../types';
    +import { clustersActions } from '@rackn/plugin-digitalrebar-backend';

     export default async function createPlugin(
       env: PluginEnvironment,
     ): Promise<Router> {
       const catalogClient = new CatalogClient({
         discoveryApi: env.discovery,
       });

    +  const integrations = ScmIntegrations.fromConfig(env.config);
    +
    +  // since we are adding actions, we must manually create the built-in actions
    +  // if you are already adding custom actions, simply add the `...clustersActions` line
    +  const actions = [
    +    ...createBuiltinActions({
    +      catalogClient,
    +      integrations,
    +      config: env.config,
    +      reader: env.reader,
    +    }),
    +    // add this following line
    +    ...clustersActions(env.config),
    +  ];
    +
       return await createRouter({
         logger: env.logger,
         config: env.config,
         reader: env.reader,
         catalogClient,
         identity: env.identity,
         permissions: env.permissions,
    +    actions,
       });
     }
    ```
1. Update `packages/backend/src/index.ts`

    Via curl: `curl https://gitlab.com/Meshiest/backstage-drp-demo/-/raw/main/patches/2-router.patch | git apply`

    Manually:
    ```diff
    @@ -31,6 +31,7 @@ import search from './plugins/search';
     import { PluginEnvironment } from './types';
     import { ServerPermissionClient } from '@backstage/plugin-permission-node';
     import { DefaultIdentityClient } from '@backstage/plugin-auth-node';
    +import digitalrebar from './plugins/digitalrebar';

     function makeCreateEnv(config: Config) {
       const root = getRootLogger();
    @@ -85,6 +86,7 @@ async function main() {
       const techdocsEnv = useHotMemoize(module, () => createEnv('techdocs'));
       const searchEnv = useHotMemoize(module, () => createEnv('search'));
       const appEnv = useHotMemoize(module, () => createEnv('app'));
    +  const drpEnv = useHotMemoize(module, () => createEnv('drp'));

       const apiRouter = Router();
       apiRouter.use('/catalog', await catalog(catalogEnv));
    @@ -93,6 +95,7 @@ async function main() {
       apiRouter.use('/techdocs', await techdocs(techdocsEnv));
       apiRouter.use('/proxy', await proxy(proxyEnv));
       apiRouter.use('/search', await search(searchEnv));
    +  apiRouter.use('/drp', await digitalrebar(drpEnv));

       // Add backends ABOVE this line; this 404 handler is the catch-all fallback
       apiRouter.use(notFoundHandler());
    ```
1. Clone DRP example plugin: `git clone https://gitlab.com/zfranks/backstage-plugin-digitalrebar plugins/backstage-plugin-digitalrebar`
1. Add it to the packages: `yarn add --cwd packages/app @rackn/plugin-digitalrebar@0.1.0`
1. Update `packages/app/src/App.tsx`

    Via curl: `curl https://gitlab.com/Meshiest/backstage-drp-demo/-/raw/main/patches/3-frontend.patch | git apply`

    Manually:
    ```diff
    @@ -33,6 +33,7 @@ import { AppRouter, FlatRoutes } from '@backstage/core-app-api';
     import { CatalogGraphPage } from '@backstage/plugin-catalog-graph';
     import { RequirePermission } from '@backstage/plugin-permission-react';
     import { catalogEntityCreatePermission } from '@backstage/plugin-catalog-common/alpha';
    +import { DigitalrebarPage } from '@rackn/plugin-digitalrebar';

     const app = createApp({
       apis,
    @@ -91,6 +92,7 @@ const routes = (
         </Route>
         <Route path="/settings" element={<UserSettingsPage />} />
         <Route path="/catalog-graph" element={<CatalogGraphPage />} />
    +    <Route path="/digitalrebar" element={<DigitalrebarPage />} />
       </FlatRoutes>
     );
    ```
1. Make scaffolder templates dir `mkdir -p packages/backend/templates/drp`
1. Create `packages/backend/templates/drp/create-cluster.yaml` with

    Via curl: `curl -o packages/backend/templates/drp/create-cluster.yaml https://gitlab.com/Meshiest/backstage-drp-demo/-/raw/main/backstage/packages/backend/templates/drp/create-cluster.yaml`

    Manually:
    ```yaml
    apiVersion: scaffolder.backstage.io/v1beta3
    kind: Template
    metadata:
      name: drp-create-cluster
      title: Create DRP Cluster
      description: Create a Digital Rebar Cluster.
    spec:
      owner: user:guest
      type: service

      parameters:
        - title: Cluster information
          required: ["name", "broker"]
          properties:
            name:
              title: Name
              type: string
              description: The name of the Cluster to add.
              ui:autofocus: true
            broker:
              title: Broker
              type: string
              description: The broker for the Cluster.
      steps:
        - id: create
          name: Create Cluster
          action: drp:clusters:create # our custom action!
          input:
            # the following fields are directly
            # passed to the created DRP object
            Name: ${{ parameters.name }}
            Params:
              broker/name: ${{ parameters.broker }}
      output:
        links:
          - title: Jump to Cluster in UX
            url: https://portal.rackn.io/#/e/${{ steps.create.output.endpoint }}/clusters/${{ steps.create.output.object.Uuid }}
    ```
1. Update `app-config.yaml` with (in `catalog.locations`)

    Via curl: `curl https://gitlab.com/Meshiest/backstage-drp-demo/-/raw/main/patches/4-template.patch | git apply`

    Manually:
    ```diff
    @@ -93,6 +93,11 @@ catalog:
           rules:
             - allow: [User, Group]

    +    - type: file
    +      target: templates/drp/create-cluster.yaml
    +      rules:
    +        - allow: [Template]
    +
         ## Uncomment these lines to add more example data
         # - type: url
         #   target: https://github.com/backstage/backstage/blob/master/packages/catalog-model/examples/all.yaml
    ```
1. Generate a token with `drpcli users token rocketskates ttl 9999999 | jq .Token -r` (ttl is in seconds)
1. Populate `app-config.local.yaml` with:
    ```yaml
    digitalrebar:
      endpoint: 127.0.0.1:8092
      token: REPLACE_ME
    ```
1. Depending on setup, tinker with the `app-config.yaml`'s listen host (to 0.0.0.0), and change some `localhost`s to `127.0.0.1`.

    Replace `localhost` with `127.0.0.1` via curl: `curl https://gitlab.com/Meshiest/backstage-drp-demo/-/raw/main/patches/5-rebind.patch | git apply`
1. Start backstage: `yarn dev`
1. Open backstage at http://localhost:3000 (unless you changed the cors host to `127.0.0.1`)