#!/usr/bin/env bash
APP_NAME=example
USERNAME=rocketskates
ENDPOINT="127.0.0.1:8092"

BACKSTAGE_APP_NAME=$APP_NAME npx @backstage/create-app@latest
cd $APP_NAME
git init
git clone https://gitlab.com/zfranks/backstage-plugin-digitalrebar-backend plugins/backstage-plugin-digitalrebar-backend
yarn add --cwd packages/backend @rackn/plugin-digitalrebar-backend@0.1.0
curl -o packages/backend/src/plugins/digitalrebar.ts https://gitlab.com/Meshiest/backstage-drp-demo/-/raw/main/backstage/packages/backend/src/plugins/digitalrebar.ts
curl https://gitlab.com/Meshiest/backstage-drp-demo/-/raw/main/patches/1-scaffolder.patch | git apply
curl https://gitlab.com/Meshiest/backstage-drp-demo/-/raw/main/patches/2-router.patch | git apply
git clone https://gitlab.com/zfranks/backstage-plugin-digitalrebar plugins/backstage-plugin-digitalrebar
yarn add --cwd packages/app @rackn/plugin-digitalrebar@0.1.0
curl https://gitlab.com/Meshiest/backstage-drp-demo/-/raw/main/patches/3-frontend.patch | git apply
mkdir -p packages/backend/templates/drp
curl -o packages/backend/templates/drp/create-cluster.yaml https://gitlab.com/Meshiest/backstage-drp-demo/-/raw/main/backstage/packages/backend/templates/drp/create-cluster.yaml
curl https://gitlab.com/Meshiest/backstage-drp-demo/-/raw/main/patches/4-template.patch | git apply
cat > app-config.local.yaml <<EOF
digitalrebar:
  endpoint: $ENDPOINT
  token: $(drpcli users token $USERNAME ttl 9999999 | jq .Token -r)
EOF
curl https://gitlab.com/Meshiest/backstage-drp-demo/-/raw/main/patches/5-rebind.patch | git apply
