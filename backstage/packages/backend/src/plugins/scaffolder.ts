import { CatalogClient } from '@backstage/catalog-client';
import { ScmIntegrations } from '@backstage/integration';
import {
  createBuiltinActions,
  createRouter,
} from '@backstage/plugin-scaffolder-backend';
import { Router } from 'express';
import type { PluginEnvironment } from '../types';
import { clustersActions } from '@rackn/plugin-digitalrebar-backend';

export default async function createPlugin(
  env: PluginEnvironment,
): Promise<Router> {
  const catalogClient = new CatalogClient({
    discoveryApi: env.discovery,
  });

  const integrations = ScmIntegrations.fromConfig(env.config);

  // since we are adding actions, we must manually create the built-in actions
  // if you are already adding custom actions, simply add the `...clustersActions` line
  const actions = [
    ...createBuiltinActions({
      catalogClient,
      integrations,
      config: env.config,
      reader: env.reader,
    }),
    // add this following line
    ...clustersActions(env.config),
  ];

  return await createRouter({
    logger: env.logger,
    config: env.config,
    database: env.database,
    reader: env.reader,
    catalogClient,
    identity: env.identity,
    permissions: env.permissions,
    actions,
  });
}
